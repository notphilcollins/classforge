
from .classes import Class as _Class
from .fields import Field as _Field

Class = _Class
Field = _Field
