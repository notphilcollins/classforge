import os

from rockchisel import Builder

path = os.path.dirname(os.path.realpath(__file__))

VERSION = 0.4

site = Builder(
	
	input_path  = path,
	output_path = os.path.join(path, "output"),
	theme = "rockchisel.themes.rockdoc",

	variables = dict(version = VERSION),
	page_title_template = "ClassForge {{ version }}: {{ title }}",
	
	index = "index",
	sections = {
			"User Guide": {
				"Home" : "index",
				"Installation": "installation",
				"Usage" : "usage",
                "Changelog" : "changelog"
			},
			"Community": {
				"License": "license",
				"Contributing": "contributing"
			}
	},
	
	theme_options = dict(
		sidebar_background = "#FF0000"
	)

)

site.build()
